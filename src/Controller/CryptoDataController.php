<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CryptoDataController extends AbstractController
{
    /**
     * Route prints out current crypto price in EUR and USD
     * Refreshes every 5 seconds, don't overcomplicate it :)
     * @return Response
     */
    #[Route(path: '/', name: 'cryptoPrice', methods: ['GET'])]
    public function list(): Response
    {
        // get value from URI:
        $data = file_get_contents($_ENV[ 'DATA_URI' ]);

        if (!$data) {
            return new Response('Data not found');
        }

        $data = json_decode($data, true);

        $printout = [
            'title' => $data[ 'chartName' ],
            'eur' => $data[ 'bpi' ][ 'EUR' ][ 'rate' ],
            'usd' => $data[ 'bpi' ][ 'USD' ][ 'rate' ],
        ];
        print_r($printout);

        // auto reload in 5 seconds
        return new Response('<meta http-equiv="refresh" content="5">');
    }
}
