<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SortArray extends AbstractController
{

    private function sort(array &$array)
    {
        $src = [];
        foreach ($array as $i => $value) {
            $num = intval(preg_replace('/[^0-9]/', '', $value));
            $src[] = ['i' => $i, 'num' => intval($num), 'str' => $value];
        }

        usort($src, function ($a, $b) {
            return $a['num'] > $b['num'];
        });

        $array = [];

        foreach ($src as $key => $value) {
            $handle = strval($value['i']);
            $array[$handle] = $value['str'];
        }
    }

    /**
     * Test sort function works as expected
     */
    public function testSort()
    {
        $arr = [
            '0' => 'ahoj1',
            '1' => 'Ahoj10',
            '2' => 'ahoj12',
            '3' => 'Ahoj2',
            '4' => 'ahoj3',
        ];
        $this->sort($arr);
        $expected = [
            '0' => 'ahoj1',
            '3' => 'Ahoj2',
            '4' => 'ahoj3',
            '1' => 'Ahoj10',
            '2' => 'ahoj12',
        ];

        array_diff($arr, $expected) === [] ? print_r('Test passed') : print_r('Test failed');
    }

    #[Route(path: '/array', name: 'array', methods: ['GET'])]
    public function list(): Response
    {
        // get value from URI:
        $arr = [
            '0' => 'ahoj1',
            '1' => 'Ahoj10',
            '2' => 'ahoj12',
            '3' => 'Ahoj2',
            '4' => 'ahoj3',
        ];

        $this->sort($arr);

        //$this->testSort();

        // write out the items and index
        return new Response(var_export($arr, true));
    }
}
